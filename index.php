<?php 
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
session_start();
if (!isset($_SESSION['user_id'])){
	header("location:auth/login.php");
}
$page=$_GET['page'];
?>

<!DOCTYPE html>
<html lang="en" class="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Toko ABC</title>

  <link rel="stylesheet" href="assets/css/main.css">

  <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png" />
  <link rel="mask-icon" href="safari-pinned-tab.svg" color="#00b4b6" />

  <meta name="description" content="TOKO ABC">

  <meta property="og:url" content="https://hiskia.app">
  <meta property="og:site_name" content="JustBoil.me">
  <meta property="og:title" content="TOKO ABC">
  <meta property="og:description" content="TOKO ABC">
  <meta property="og:image" content="https://hiskia.app">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1920">
  <meta property="og:image:height" content="960">

  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:title" content="TOKO ABC">
  <meta property="twitter:description" content="TOKO ABC">
  <meta property="twitter:image:src" content="https://hiskia.app">
  <meta property="twitter:image:width" content="1920">
  <meta property="twitter:image:height" content="960">

</head>

<body>

  <div id="app">

    <nav id="navbar-main" class="navbar is-fixed-top">
      <div class="navbar-brand">
        <a class="navbar-item mobile-aside-button">
          <span class="icon"><i class="mdi mdi-forwardburger mdi-24px"></i></span>
        </a>
        <div class="navbar-item">

        </div>
      </div>
      <div class="navbar-brand is-right">
        <a class="navbar-item --jb-navbar-menu-toggle" data-target="navbar-menu">
          <span class="icon"><i class="mdi mdi-dots-vertical mdi-24px"></i></span>
        </a>
      </div>
      <div class="navbar-menu" id="navbar-menu">
        <div class="navbar-end">
          <div class="navbar-item dropdown has-divider has-user-avatar">
            <a class="navbar-link">
              <div class="user-avatar">
                <img src="https://avatars.dicebear.com/v2/initials/<?php echo strtolower($_SESSION['user_name']) ?>.svg" alt="John Doe" class="rounded-full">
              </div>
              <div class="is-user-name"><span><?php echo $_SESSION['user_name']; ?></span></div>
              <span class="icon"><i class="mdi mdi-chevron-down"></i></span>
            </a>
            <div class="navbar-dropdown">
              <hr class="navbar-divider">
              <a class="navbar-item" href="auth/logout.php">
                <span class="icon"><i class="mdi mdi-logout"></i></span>
                <span>Log Out</span>
              </a>
            </div>
          </div>
          <a href="auth/logout.php" title="Log out" class="navbar-item desktop-icon-only">
            <span class="icon"><i class="mdi mdi-logout"></i></span>
            <span>Log out</span>
          </a>
        </div>
      </div>
    </nav>

    <aside class="aside is-placed-left is-expanded">
      <div class="aside-tools">
        <div>
          Toko <b class="font-black">ABC</b>
        </div>
      </div>
      <div class="menu is-menu-main">
        <p class="menu-label">General</p>
        <ul class="menu-list">
          <li class="">
            <a href="index.php?page=home">
              <span class="icon"><i class="mdi mdi-desktop-mac"></i></span>
              <span class="menu-item-label">Dashboard</span>
            </a>
          </li>
        </ul>
        <p class="menu-label">Master Data</p>
        <ul class="menu-list">
          <li class="--set-active-tables-html">
            <a href="index.php?page=users">
              <span class="icon"><i class="mdi mdi-emoticon"></i></span>
              <span class="menu-item-label">User</span>
            </a>
          </li>
          <li class="--set-active-forms-html">
            <a href="index.php?page=customers">
              <span class="icon"><i class="mdi mdi-bank"></i></span>
              <span class="menu-item-label">Pelanggan</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>

    <?php
      if(!$page){
          echo "<script>document.location.href='index.php?page=home'</script>";
      }else{
        if(!@include("contents/$page.php")) {
          echo '<center style="font-size:40px;margin-top:25%;margin-bottom:25%">Page not found 👋</center>';
        }
      }
    ?>

    <footer class="footer">
      <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0">
        <div class="flex items-center justify-start space-x-3">
          <div>
            © 2022, Hiskia
          </div>
        </div>
      </div>
    </footer>

  </div>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
  <script type="text/javascript" src="assets/js/chart.sample.min.js"></script>

  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css">

</body>

</html>
