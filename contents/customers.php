<?php
include "systems/DB.php";
$customers=$db->get("customers.*, users.name as user_name","customers","LEFT JOIN users ON users.id = customers.user_id ORDER BY id ASC");
?>
<section class="is-title-bar">
  <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
    <ul>
      <li>Admin</li>
      <li>Pelanggan</li>
    </ul>
  </div>
</section>

<section class="is-hero-bar">
  <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
    <h1 class="title">
      Master Pelanggan
    </h1>
    <a href="index.php?page=customer_form" class="button light">Tambah Pelanggan</a>
  </div>
</section>

<section class="section main-section">
  <div class="card has-table">
    <header class="card-header">
      <p class="card-header-title">
        <span class="icon"><i class="mdi mdi-account-multiple"></i></span>
        Pelanggan
      </p>
      <a href="index.php?page=customers" class="card-header-icon">
        <span class="icon"><i class="mdi mdi-reload"></i></span>
      </a>
    </header>
    <div class="card-content">
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Telp</th>
            <th>User</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($customers as $customer){
          ?>
          <tr>
            <td><?php echo $customer['id'];?></td>
            <td><?php echo $customer['name'];?></td>
            <td><?php echo $customer['address'];?></td>
            <td><?php echo $customer['telp'];?></td>
            <td><?php echo $customer['user_name'];?></td>
            <td class="actions-cell">
              <div class="buttons right nowrap">
                <a href="index.php?page=customer_form&id=<?php echo $customer['id'] ?>" class="button small green --jb-modal"
                  data-target="sample-modal-2">
                  <span class="icon"><i class="mdi mdi-pencil"></i></span>
                </a>
                <a href="actions/customers.php?action=delete&id=<?php echo $customer['id'] ?>"
                  class="button small red --jb-modal" data-target="sample-modal" type="button">
                  <span class="icon"><i class="mdi mdi-trash-can"></i></span>
                </a>
              </div>
            </td>
          </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
      <?php if($customers->rowCount() == 0) { ?>
        <div class="card empty">
          <div class="card-content">
            <div>
              <span class="icon large"><i class="mdi mdi-emoticon-sad mdi-48px"></i></span>
            </div>
            <p>Data masih kosong</p>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>