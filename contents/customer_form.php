<?php
include "systems/DB.php";
$id = isset($_GET['id']) ? $_GET['id'] : '';
$query=$db->get("*","customers","WHERE id='$id'");
$data=$query->fetch();
$user_id = isset($data['user_id']) ? $data['user_id'] : '';
$users = $db->get("*","users","ORDER BY id ASC");
?>

<section class="is-title-bar">
    <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
        <ul>
            <li>Admin</li>
            <li>Pelanggan Form</li>
        </ul>
    </div>
</section>

<section class="is-hero-bar">
    <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
        <h1 class="title">
            Pelanggan
        </h1>
        <a href="index.php?page=customers" class="button light">Back</a>
    </div>
</section>

<section class="section main-section">
    <div class="card mb-6">
        <header class="card-header">
            <p class="card-header-title">
                <span class="icon"><i class="mdi mdi-ballot"></i></span>
                Form
            </p>
        </header>
        <div class="card-content">
            <form method="post" action="actions/customers.php">
                <input type="hidden" name="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>">
                <input type="hidden" name="action" value="<?php echo isset($_GET['id']) ? 'update' : 'insert' ?>">
                <div class="field">
                    <div class="field-body">
                        <div class="field">
                            <div class="control icons-left">
                                <input class="input" type="text" placeholder="Name" autocomplete="off" required
                                    name="name" value="<?php echo isset($data['name']) ? $data['name'] : '';?>">
                                <span class="icon left"><i class="mdi mdi-account"></i></span>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control icons-left icons-right">
                                <textarea required name="address" class="textarea" placeholder="Alamat"><?php echo isset($data['address']) ? $data['address'] : '';?></textarea>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control icons-left icons-right">
                                <input class="input" type="text" placeholder="Telp" value="<?php echo isset($data['telp']) ? $data['telp'] : '';?>" name="telp"
                                    autocomplete="off" required>
                                <span class="icon left"><i class="mdi mdi-plus"></i></span>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select">
                                    <select name="user_id" required>
                                        <option value="" disabled selected>-- Pilih User</option>
                                        <?php
                                            foreach($users as $user){
                                        ?>
                                        <option value="<?php echo $user['id'] ?>" <?php echo ($user_id == $user['id']) ? 'selected' : '';?>><?php echo $user['name'] ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field grouped">
                            <div class="control">
                                <button type="submit" class="button green">
                                    Submit
                                </button>
                            </div>
                            <div class="control">
                                <button type="reset" class="button red">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>