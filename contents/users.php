<?php
include "systems/DB.php";
$users=$db->get("*","users","ORDER BY id ASC");
?>
<section class="is-title-bar">
  <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
    <ul>
      <li>Admin</li>
      <li>User</li>
    </ul>
  </div>
</section>

<section class="is-hero-bar">
  <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
    <h1 class="title">
      Master User
    </h1>
    <a href="index.php?page=user_form" class="button light">Tambah User</a>
  </div>
</section>

<section class="section main-section">
  <div class="card has-table">
    <header class="card-header">
      <p class="card-header-title">
        <span class="icon"><i class="mdi mdi-account-multiple"></i></span>
        User
      </p>
      <a href="index.php?page=users" class="card-header-icon">
        <span class="icon"><i class="mdi mdi-reload"></i></span>
      </a>
    </header>
    <div class="card-content">
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Telp</th>
            <th>Privilege</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($users as $user){
          ?>
          <tr>
            <td><?php echo $user['id'];?></td>
            <td><?php echo $user['name'];?></td>
            <td><?php echo $user['email'];?></td>
            <td><?php echo $user['telp'];?></td>
            <td><?php echo $user['privilege'];?></td>
            <td class="actions-cell">
              <div class="buttons right nowrap">
                <a href="index.php?page=user_form&id=<?php echo $user['id'] ?>" class="button small green --jb-modal"
                  data-target="sample-modal-2">
                  <span class="icon"><i class="mdi mdi-pencil"></i></span>
                </a>
                <a href="actions/users.php?action=delete&id=<?php echo $user['id'] ?>"
                  class="button small red --jb-modal" data-target="sample-modal" type="button">
                  <span class="icon"><i class="mdi mdi-trash-can"></i></span>
                </a>
              </div>
            </td>
          </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
      <?php if($users->rowCount() == 0) { ?>
      <div class="card empty">
        <div class="card-content">
          <div>
            <span class="icon large"><i class="mdi mdi-emoticon-sad mdi-48px"></i></span>
          </div>
          <p>Data masih kosong</p>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</section>