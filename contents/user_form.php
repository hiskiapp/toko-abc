<?php
include "systems/DB.php";
$id = isset($_GET['id']) ? $_GET['id'] : '';
$query=$db->get("*","users","WHERE id='$id'");
$data=$query->fetch();
$privilege = isset($data['privilege']) ? $data['privilege'] : '';
?>

<section class="is-title-bar">
    <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
        <ul>
            <li>Admin</li>
            <li>User Form</li>
        </ul>
    </div>
</section>

<section class="is-hero-bar">
    <div class="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
        <h1 class="title">
            User
        </h1>
        <a href="index.php?page=users" class="button light">Back</a>
    </div>
</section>

<section class="section main-section">
    <div class="card mb-6">
        <header class="card-header">
            <p class="card-header-title">
                <span class="icon"><i class="mdi mdi-ballot"></i></span>
                Form
            </p>
        </header>
        <div class="card-content">
            <form method="post" action="actions/users.php">
                <input type="hidden" name="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>">
                <input type="hidden" name="action" value="<?php echo isset($_GET['id']) ? 'update' : 'insert' ?>">
                <div class="field">
                    <div class="field-body">
                        <div class="field">
                            <div class="control icons-left">
                                <input class="input" type="text" placeholder="Name" autocomplete="off" required
                                    name="name" value="<?php echo isset($data['name']) ? $data['name'] : '';?>">
                                <span class="icon left"><i class="mdi mdi-account"></i></span>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control icons-left icons-right">
                                <input class="input" type="email" placeholder="Email"
                                    value="<?php echo isset($data['email']) ? $data['email'] : '';?>" required
                                    name="email" autocomplete="off">
                                <span class="icon left"><i class="mdi mdi-mail"></i></span>
                                <span class="icon right"><i class="mdi mdi-check"></i></span>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control icons-left icons-right">
                                <input class="input" type="password" placeholder="Password" value="" name="password"
                                    autocomplete="off" <?php echo ($id) ? '' : 'required' ?>>
                                <span class="icon left"><i class="mdi mdi-key"></i></span>
                                <span class="icon right"><i class="mdi mdi-check"></i></span>
                            </div>
                            <?php if($id){ ?>
                            <span class="text-grey text-sm">Kosongkan password jika tidak ingin mengubah password</span>
                            <?php } ?>
                        </div>
                        <div class="field">
                            <div class="control icons-left icons-right">
                                <input class="input" type="text" placeholder="Telp" value="<?php echo isset($data['telp']) ? $data['telp'] : '';?>" name="telp"
                                    autocomplete="off" required>
                                <span class="icon left"><i class="mdi mdi-plus"></i></span>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select">
                                    <select name="privilege" required>
                                        <option value="" disabled selected>-- Pilih Peran</option>
                                        <option <?php echo ($privilege == 'Admin') ? 'selected' : '';?>>Admin</option>
                                        <option <?php echo ($privilege == 'Marketing') ? 'selected' : '';?>>Marketing
                                        </option>
                                        <option <?php echo ($privilege == 'Sales') ? 'selected' : '';?>>Sales</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field grouped">
                            <div class="control">
                                <button type="submit" name="<?php echo $action; ?>" value="" class="button green">
                                    Submit
                                </button>
                            </div>
                            <div class="control">
                                <button type="reset" class="button red">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>