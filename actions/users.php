
<?php
include "../systems/DB.php";
$id = $_POST['id'] ?: $_GET['id'];
$name = $_POST['name'];
$email = $_POST['email'];
$password = password_hash($_POST["password"], PASSWORD_DEFAULT);
$telp = $_POST['telp'];
$privilege = $_POST['privilege'];
$action = $_POST['action'] ?: $_GET['action'];

if ($action == 'insert') {
    $insert = $db->insert("users (name, email, password, telp, privilege)","'$name', '$email', '$password', '$telp', '$privilege'");
    if($insert){
        echo "<script>alert('Berhasil Disimpan')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }else{
        echo "<script>alert('Gagal Disimpan')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }
}elseif ($action == 'update') {
    $values = "name='$name', email='$email', telp='$telp', privilege='$privilege'";
    if(isset($_POST['password'])){
        $values .= ", password='$password'";
    }
    
    $update=$db->update("users",$values,"id='$id'" );
    if($update){
        echo "<script>alert('Berhasil Diupdate')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }else{
        echo "<script>alert('Gagal Diupdate')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }
}elseif ($action == 'delete') {
    $delete=$db->delete("users","id='$id'");
    if($delete){
        echo "<script>alert('Data Berhasil Dihapus')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }else{
        echo "<script>alert('Gagal Dihapus')</script>";
        echo "<script>document.location.href='../index.php?page=users'</script>";
    }
}else {
    echo "<script>alert('Action not detected')</script>";
    echo "<script>document.location.href='../index.php?page=users'</script>";
}
?>